#ifndef SHARE_H
#define SHARE_H

/**
 * @file share.h
 *
 * Shared functions called by SDFS client and server
 *
 * @author Jiaqi Liu
*/

#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include <netdb.h>
#include <netinet/in.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include <algorithm>
#include <fstream>
#include <iostream>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <vector>
using namespace std;

/**
 * @def PUT_PORT
 * @brief The port client will be connecting to for put operation
 */
#define PUT_PORT "3492"
/**
 * @def GET_PORT
 * @brief The port client will be connecting to for get operation
 */
#define GET_PORT "3493"
/**
 * @def DELETE_PORT
 * @brief The port client will be connecting to for delete operation
 */
#define DELETE_PORT "3494"
/**
 * @def BROADCAST_PORT
 * @brief The port used to broadcast file list
 */
#define BROADCAST_PORT "3495"
/**
 * @def JOIN_PORT
 * @brief The port a new node will be connecting to when the node joins cluster
 */
#define JOIN_PORT "3496"
/**
 * @def REPLICATE_PORT
 * @brief Replicate lost files(handle node failure) 
 */
#define REPLICATE_PORT "3497"

/**
 * @def BUFFER_LENGTH
 * @brief The max size of buffer that holds socket message
 */
#define BUFFER_LENGTH 60000

/**
 * @def NUM_REPLICA
 * @brief Number of replicas of each file in cluster
 */
#define NUM_REPLICA 3

/**
 * Send local_filename to SDFS so that the enture cluster has 3 replicas of this file
 *
 * @param local_filename name of the file in local file system that is to be stored in SDFS
 * @param sdfs_filename new name of the file in SDFS
 * @param whether the file will be fetched from local file system or SDFS
 *
 * @todo add unit test
 */
void put(const char * local_filename, const char * sdfs_filename, bool from_local);

/**
 * Read file list on the node that calls this function and
 * deserialize it to a map, in which the key is the IP address of other nodes, including itself, and the value is the name of files that stored on each other node
 *
 * @param file_list a dictionary that stores the mapping from the IP to the files
 *
 * @todo add unit test
 */
void get_file_list(map< string, vector<string> > & file_list);

/**
 * Read file list using a read lock. While this function is executing, no one can write to file list
 *
 * @param raw the string variable that holds the content of the file list
 *
 * @todo add unit test
 */
void read_file_list(string & raw);

/**
 * Obtain a list of PUT target(IP).
 * If there are alrady enough replicas in the cluster, target will be empty.
 *
 * @param put_target a list of target who will recieve file PUT request and store the file on it
 * @param file_list a mapping from IP(machine) to list of files stored on that IP
 * @param sdfs_filename the name of the file in SDFS once it's been stored in SDFS
 *
 * @todo add unit test
 */
void get_put_target(vector<string> & put_target, map< string, vector<string> > & file_list, const char * sdfs_filename);

/**
 * Sort VM IP addresses by number of files on VM
 *
 * @param target_candidate a list(vector) of VM IP addresses that is to be sorted
 * @param file_list a mapping from IP(machine) to all the name of files stored on that IP
 * @param key the IP to be inserted so that the VM IP addresses remain sorted
 * @param value the number of files stored on the IP("key")
 *
 * @todo add unit test
 */
void inorder_insert(vector<string> & target_candidate, map< string, vector<string> > & file_list, string key, size_t value);

/**
 * Construct TCP connection to a remote server.
 *
 * @param socket_file_descriptor the socket in the connection
 * @param IP the IP address of the remote server
 * @param port the port number of the remote server
 *
 * @todo add unit test
 */
void make_socket(int & socket_file_descriptor, string & IP, const char * port);

/**
 * Send a file to remote host in SDFS.
 *
 * @param socket_file_descriptor the socket that transmits the file
 * @param local_filename the name of the file on local host
 * @param sdfs_filename the name of the file in SDFS
 * @param whether the file will be fetched from local file system or SDFS
 *
 * @todo add unit test
 */
void send_file(int & socket_file_descriptor, const char * local_filename, const char * sdfs_filename, bool from_local);

/**
 * Flush most updated file list to disk
 *
 * @param file_list the most update file list in memory
 *
 * @todo add unit test
 */
void update_file_list(map< string, vector<string> > & file_list);

/**
 * Broadcast updated file list to all alive nodes in cluster
 *
 * @param file_list the file list to be broadcasted
 *
 * @todo add unit test
 */
void broadcast_file_list(map< string, vector<string> > & file_list);

/**
 * Send local file list to a remote host.
 * The sender is local host. The receiver of the file list is the other end of the specified "socket_file_descriptor"
 *
 * @param socket_file_descriptor the socket between the local host and the remote host that receives the file list
 *
 * @todo add unit test
 */
void send_file_list(int & socket_file_descriptor);

/**
 * Obtain the IP address of the local machine
 *
 * @return the IP address of the local machine
 *
 * @todo add unit test
 */
string get_selfIP();

/**
 * Print the in-memory file list.
 *
 * @param file_list the file list to be printed
 * @param identifier the ID that identifies each call to this function
 *
 * @todo add unit test
 */
void print_file_list(map< string, vector<string> > & file_list, string identifier);

#endif
