#!/bin/bash


# ************************************************ #
# distribute photon source code to all other nodes #
# ************************************************ #

print_usage()
{
	echo "Usasge: $0 [--IP_file=<string>] [--replicator_IP=<string>]" 1>&2;
	exit 1;
}

for argument in "$@"; do
	case $argument in
		--IP_file=*)
			IP_file=$(cut -d "=" -f 2 <<< $argument)
			shift
			;;
		--replicator_IP=*)
			replicator_IP=$(cut -d "=" -f 2 <<< $argument)
			shift
			;;
		*)
			print_usage
			;;
	esac
done

if [ -z "${IP_file}" ] || [ -z "${replicator_IP}" ]; then
	print_usage
fi

rm -rf ../service < /dev/null
cp -r service ../

while IFS= read -r IP; do
	echo "$IP"
	ssh "$IP" "rm -rf service" < /dev/null
	scp -r ./service "$IP":
	printf "$IP" > selfIP.txt
	scp selfIP.txt "$IP":~/service/logger/
	rm selfIP.txt
done < "${IP_file}"

printf "$replicator_IP" > ~/service/logger/selfIP.txt

rm -rf ~/batch_processing
cp -r batch_processing ~
