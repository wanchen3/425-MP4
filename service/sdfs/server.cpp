/**
 * @file server.cpp
 *
 * SDFS server
 *
 * @author Jiaqi Liu
 */

#include "share.h"

/**
 * @def BACKLOG
 * @brief the max number of pending connections to this server
 */
#define BACKLOG 10

/**
 * @def PATH_LENGTH
 * @brief the length of requested file path (GET & DELETE)
 */
#define PATH_LENGTH 600

/**
 * Initialize a socket at specified port on this server
 *
 * @param socket_file_descriptor the socket to be initialized
 * @param port the port number
 */
void make_socket(int & socket_file_descriptor, const char * port)
{
	struct addrinfo hints;
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;

	int result;
	struct addrinfo * server_information;
	if((result = getaddrinfo(NULL, port, &hints, &server_information)) != 0)
	{
		fprintf(stderr, "getaddrinfo(server make_socket()): %s\n", gai_strerror(result));
		exit(1);
	}

	/** loop through all the results and bind to the first we can */
	struct addrinfo * temp;
	int yes = 1;
	for(temp = server_information; temp != NULL; temp = temp->ai_next)
	{
		if((socket_file_descriptor = socket(temp->ai_family, temp->ai_socktype, temp->ai_protocol)) == -1)
		{
			perror("sdfs_server:make_socket - socket");
			continue;
		}
		if(setsockopt(socket_file_descriptor, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1)
		{
			perror("sdfs_server:make_socket - setsockopt");
			exit(1);
		}
		if(bind(socket_file_descriptor, temp->ai_addr, temp->ai_addrlen) == -1)
		{
			if(close(socket_file_descriptor) == -1)
			{
				perror("sdfs_server:make_socket - close");
				exit(1);
			}
			perror("sdfs_server:make_socket - bind");
			continue;
		}

		break;
	}

	freeaddrinfo(server_information); // all done with this structure

	if(temp == NULL)
	{
		fprintf(stderr, "sdfs_server:make_socket - failed to bind\n");
		exit(1);
	}

	// tell a socket to listen for incoming connections
	if(listen(socket_file_descriptor, BACKLOG) == -1)
	{
		perror("sdfs_server:make_socket - listen");
		exit(1);
	}
}

/**
 * Find the value of the socket with the largest file descriptor value among a set of sockets.
 *
 * @param socket_put PUT socket
 * @param socket_get GET socket
 * @param socket_broadcast boradcast socket
 * @param socket_join socket that newly joined node will contact
 * @param socket_replication socket for replicate SDFS files
 * 
 * @return the value of the socket with the largest file descriptor value
 */
int find_max(int socket_put, int socket_get, int socket_delete, int socket_broadcast, int socket_join, int socket_replicate)
{
	int retval = socket_put;
	(retval < socket_get) && (retval = socket_get);
	(retval < socket_delete) && (retval = socket_delete);
	(retval < socket_broadcast) && (retval = socket_broadcast);
	(retval < socket_join) && (retval = socket_join);
	(retval < socket_replicate) && (retval = socket_replicate);
	return retval;
}

/**
 * Load file into SDFS
 *
 * @param socket_file_descriptor the socket that receives incoming file content
 */
void load(int & socket_file_descriptor)
{
	struct sockaddr_storage their_addr; // connector's address information
	socklen_t sin_size = sizeof their_addr;
	int new_connection = accept(socket_file_descriptor, (struct sockaddr *)&their_addr, &sin_size);
	if(new_connection == -1)
	{
		perror("sdfs_server:load - accept");
		exit(1);
	}
	if(!fork())
	{
		if(close(socket_file_descriptor) == -1) // child doesn't need the listener
		{
			perror("sdfs_server:load - close");
			exit(1);
		}

		FILE * file_load;

		char buffer[BUFFER_LENGTH];
		memset(&buffer, 0, sizeof buffer);
		size_t num_recv;
		bool flag = 0;
		while((num_recv = recv(new_connection, buffer, BUFFER_LENGTH-1, 0)) > 0)
		{
			if(!flag)
			{
				char * separator = strstr(buffer, ":");
				*separator = '\0';
				if((file_load = fopen(("./sdfs_file/"+string(buffer)).c_str(), "wb")) == NULL)
				{
					perror("fopen");
					exit(1);
				}
				separator = separator + sizeof(char);
				fwrite(separator, sizeof(char), num_recv-strlen(buffer)-1, file_load);
				
				flag = !flag;
			}
			else
			{
				fwrite(buffer, sizeof(char), num_recv, file_load);
			}
			memset(&buffer, 0, sizeof buffer);
		}

		if(fclose(file_load) == EOF)
		{
			perror("sdfs_server:load - fclose");
			exit(1);
		}

		if(close(new_connection) == -1)
		{
			perror("sdfs_server:load - close in child");
			exit(1);
		}
		exit(0);
	}
	if(close(new_connection) == -1)  // parent doesn't need this
	{
		perror("sdfs_server:load - close in parent");
		exit(1);
	}
}

/**
 * Pull file from SDFS and send it to client.
 *
 * @param socket_file_descriptor the socket used to send SDFS file
 */
void pull(int & socket_file_descriptor)
{
	struct sockaddr_storage their_addr; // connector's address information
	socklen_t sin_size = sizeof their_addr;
	int new_connection = accept(socket_file_descriptor, (struct sockaddr *)&their_addr, &sin_size);
	if(new_connection == -1)
	{
		perror("sdfs_server:pull - accept");
		exit(1);
	}
	if(!fork())
	{
		if(close(socket_file_descriptor) == -1) // child doesn't need the listener
		{
			perror("sdfs_server:pull - close listener in child");
			exit(1);
		}

		/** get name of the file that is to be sent to client */
		char path[PATH_LENGTH];
		memset(&path, 0, sizeof path);
		ssize_t num_recv;
		if((num_recv = recv(new_connection, path, PATH_LENGTH, 0)) == -1)
		{
			perror("sdfs_server:pull - recv file name");
			exit(2);
		}
		path[num_recv] = '\0';

		/** send the file to client */
		FILE * file_get;
		if(( file_get = fopen(("./sdfs_file/"+ string(path)).c_str(), "rb") ) == NULL)
		{
			/** @todo handle the case when file does not exist in SDFS */
		}
		char buffer[BUFFER_LENGTH];
		memset(&buffer, 0, sizeof buffer);
		int num_read;
		int sent;
		while( (num_read = fread(buffer, sizeof(char), BUFFER_LENGTH, file_get)) > 0)
		{
			if(num_read < BUFFER_LENGTH)
			{
				sent = send(new_connection, buffer, num_read, 0);
				string content = buffer;
				if(sent == -1)
				{
					perror("sdfs_server:pull - send last packet");
				}
				break;
			}
			sent = send(new_connection, buffer, BUFFER_LENGTH, 0);
			if(sent == -1)
			{
				perror("sdfs_server:pull - send");
			}
		}

		fclose(file_get);
		if(close(new_connection) == -1)
		{
			perror("sdfs_server:pull - close in child");
			exit(1);
		}
		exit(0);
	}
	if(close(new_connection) == -1) // parent doesn't need this
	{
		perror("sdfs_server:pull - close in parent");
		exit(1);
	}
}

/**
 * Delete file in SDFS of this machine
 *
 * @param socket_file_descriptor 
 */
void remove(int & socket_file_descriptor)
{
	struct sockaddr_storage their_addr; // connector's address information
	socklen_t sin_size = sizeof their_addr;
	int new_connection = accept(socket_file_descriptor, (struct sockaddr *)&their_addr, &sin_size);
	if(new_connection == -1)
	{
		perror("sdfs_server:remove - accept");
		exit(1);
	}

	if(!fork())
	{
		if(close(socket_file_descriptor) == -1) // child doesn't need the listener
		{
			perror("sdfs_server:remove - close listner in child");
			exit(1);
		}

		/** Get filename to be deleted from SDFS */
		char path[PATH_LENGTH];
		memset(&path, 0, sizeof path);
		ssize_t num_recv;
		if((num_recv = recv(new_connection, path, PATH_LENGTH, 0)) == -1)
		{
			perror("recv");
			exit(2);
		}
		path[num_recv] = '\0';

		/** Delete file */
		if(remove(("./sdfs_file/"+ string(path)).c_str()) != 0)
		{
			perror("sdfs_server:remove - remove call");
			exit(1);
		}

		if(close(new_connection) == -1)
		{
			perror("sdfs_server:remove - close in child");
			exit(1);
		}
		exit(0);
	}
	if(close(new_connection) == -1)  // parent doesn't need this
	{
		perror("sdfs_server:remove - close in parent");
		exit(1);
	}
}

/**
 * Replace local file list
 *
 * @param socket_file_descriptor the socket that recieves new file list
 * @param filelist_initialized a flag value indicating wether file list has been initialized
 */
void replace_file_list(int & socket_file_descriptor, bool & filelist_initialized)
{
	struct sockaddr_storage their_addr; // connector's address information
	socklen_t sin_size = sizeof their_addr;
	int new_connection = accept(socket_file_descriptor, (struct sockaddr *)&their_addr, &sin_size);
	if(new_connection == -1)
	{
		perror("sdfs_server:replace_file_list - accept");
		exit(1);
	}
	if(!fork())
	{
		if(close(socket_file_descriptor) == -1) // child doesn't need the listener
		{
			perror("sdfs_server:replace_file_list - close listener in child");
			exit(1);
		}

		/** setup write-lock */
		struct flock file_lock;
		file_lock.l_type = F_WRLCK;
		file_lock.l_whence = SEEK_SET;
		file_lock.l_start = 0;
		file_lock.l_len = 0;
		file_lock.l_pid = getpid();

		/** acquire lock and open file */
		int new_file_list;
		if((new_file_list = open("../logger/file_list.txt", O_WRONLY | O_TRUNC)) == -1)
		{
			perror("sdfs_server:replace_file_list - open file_list.txt");
			exit(1);
		}
		if(fcntl(new_file_list, F_SETLKW, &file_lock) == -1)
		{
			perror("sdfs_server:replace_file_list - fcntl");
			exit(1);
		}

		/** write new file list to disk */
		char buffer[BUFFER_LENGTH];
		memset(&buffer, 0, sizeof buffer);
		size_t num_recv;
		while((num_recv = recv(new_connection, buffer, BUFFER_LENGTH-1, 0)) > 0)
		{
			write(new_file_list, buffer, num_recv);
			memset(&buffer, 0, sizeof buffer);
		}

		/** close file and release lock */
		file_lock.l_type = F_UNLCK;
		if(fcntl(new_file_list, F_SETLK, &file_lock) == -1)
		{
			perror("sdfs_server:replace_file_list - fcntl");
			exit(1);
		}
		if(close(new_file_list) == -1)
		{
			perror("sdfs_server:replace_file_list - close file list");
			exit(1);
		}

		if(close(new_connection) == -1)
		{
			perror("sdfs_server:replace_file_list - close in child");
			exit(1);
		}
		exit(0);
	}
	if(close(new_connection) == -1)  // parent doesn't need this
	{
		perror("sdfs_server:replace_file_list - close in parent");
		exit(1);
	}

	if(!filelist_initialized)
	{
		filelist_initialized = 1;
		cout << "OK - SDFS service started" << endl;
	}
}

/**
 * Update local membership list on disk
 *
 * @param file_list the most updated in-memory file list that is to be flushed to disk
 */
void update_membrship_list(map< string, vector<string> > & file_list)
{
	/** setup write-lock */
	struct flock fl;
	int fd;
	fl.l_type = F_WRLCK;
	fl.l_whence = SEEK_SET;
	fl.l_start = 0;
	fl.l_len = 0;
	fl.l_pid = getpid();

	/** open file with lock and write */
	fd = open("../logger/member.txt", O_WRONLY | O_TRUNC);
	fcntl(fd, F_SETLKW, &fl); // wait and grap write lock

	for(map< string, vector<string> >::iterator iter = file_list.begin(); iter != file_list.end(); iter++)
	{
		write(fd, (iter->first).c_str(), (iter->first).size());
	}

	fl.l_type = F_UNLCK;
	fcntl(fd, F_SETLK, &fl); // release lock
	close(fd);
}

/**
 * When node joins, this function update and boradcase local file list.
 *
 * @param socket_file_descriptor the socket used to receive IP address of newly joined nodes
 */
void onboard(int & socket_file_descriptor)
{
	struct sockaddr_storage their_addr; // connector's address information
	socklen_t sin_size = sizeof their_addr;
	int new_connection = accept(socket_file_descriptor, (struct sockaddr *)&their_addr, &sin_size);
	if(new_connection == -1)
	{
		perror("sdfs_server:onboard - accept");
		exit(1);
	}

	if(!fork())
	{
		if(close(socket_file_descriptor) == -1) // child doesn't need the listener
		{
			perror("sdfs_server:onboard - close listener in child");
			exit(1);
		}

		char buffer[BUFFER_LENGTH];
		memset(&buffer, 0, sizeof buffer);
		size_t num_recv;
		bool flag = 0;
		string new_member;
		if((num_recv = recv(new_connection, buffer, BUFFER_LENGTH-1, 0)) == -1)
		{
			perror("sdfs_server:onboard - recv");
			exit(1);
		}
		else
		{
			buffer[num_recv] = '\0';
			new_member = string(buffer);
		}

		if(close(new_connection) == -1)
		{
			perror("sdfs_server:onboard - close in child");
			exit(1);
		}

		/** update and broadcast file list */
		map< string, vector<string> > file_list;
		get_file_list(file_list);
		vector<string> no_file;
		file_list[new_member] = no_file;
		update_file_list(file_list);
		broadcast_file_list(file_list);
		update_membrship_list(file_list);

		exit(0);
	}
	if(close(new_connection) == -1)  // parent doesn't need this
	{
		perror("sdfs_server:onboard - close in parent");
		exit(1);
	}
}

/**
 * Get a list of alive nodes
 *
 * @param alive_IP a list to be filled with IP addresses of the alive nodes
 */
void get_alive_IP(vector<string> & alive_IP)
{
	/** set up read lock */
	struct flock file_lock;
	file_lock.l_type = F_RDLCK;
	file_lock.l_whence = SEEK_SET;
	file_lock.l_start = 0;
	file_lock.l_len = 0;
	file_lock.l_pid = getpid();

	/** open membership list file with lock */
	int input;
	if((input = open("../logger/member.txt", O_RDONLY)) == -1)
	{
		perror("sdfs_server:get_alive_IP - open member.txt");
		exit(1);
	}
	if(fcntl(input, F_SETLKW, &file_lock) == -1)
	{
		perror("sdfs_server:get_alive_IP - fcntl");
		exit(1);
	}

	/** put file content into a C++ string called "raw"(easier to manipulate) */
	string raw = "";
	int buffer_size = 18;
	char buffer[buffer_size];
	memset(&buffer, 0, sizeof buffer);
	ssize_t num_read;
	while(1)
	{
		num_read = read(input, buffer, buffer_size);
		if(num_read == -1)
		{
			perror("sdfs_server:get_alive_IP - read");
			exit(1);
		}
		if(num_read > 0)
		{
			buffer[num_read] = '\0';
			raw = raw + string(buffer);
			memset(&buffer, 0, sizeof buffer);
		}
		else
		{
			break;
		}
	}

	/** Unlock and close input file */
	file_lock.l_type = F_UNLCK;
	if(fcntl(input, F_SETLK, &file_lock) == -1)
	{
		perror("sdfs_server:get_alive_IP - fcntl");
		exit(1);
	}
	if(close(input) == -1)
	{
		perror("sdfs_server:get_alive_IP - close");
		exit(1);
	}

	// fill the vector with the C++ string
	istringstream raw_stream(raw);
	string IP;
	while(getline(raw_stream, IP, '\n'))
	{
		alive_IP.push_back(IP);
	}
}

/**
 * Initizlize local file list.\n
 * If the machine is the only node in cluster, no request will be sent over the nextwork, otherwise, it contacts the an alive node already existed in the cluster before and download file list from that node
 *
 * @param filelist_initialized a flag indicating whether the local file list has been initialized
 */
void request_file_list(bool & filelist_initialized)
{
	/** get membership list */
	vector<string> alive_IP;
	while(alive_IP.size() == 0)
	{
		get_alive_IP(alive_IP);
	}

	/** use membership list to initialize or download file list from other VM */
	if(alive_IP.size() == 1) // I am the first VM joining cluster
	{
		map< string, vector<string> > file_list;
		vector<string> no_file;
		file_list[ alive_IP[0] ] = no_file;
		update_file_list(file_list);
		filelist_initialized = 1;
		cout << "OK - SDFS service started" << endl;
	}
	else // downlaod file list: contact other VM about my join and let the VM update and broadcase its new file_list
	{
		string selfIP = get_selfIP(); // get local IP address
		for(size_t i = 0; i < alive_IP.size(); i = i+1)
		{
			if(selfIP.compare(alive_IP[i]) == 0)
			{
				continue;
			}
			else
			{
				int socket_file_descriptor;
				make_socket(socket_file_descriptor, alive_IP[i], JOIN_PORT);
				if(send(socket_file_descriptor, selfIP.c_str(), selfIP.size(), 0) == -1)
				{
					perror("sdfs_server:request_file_list - send");
					exit(1);
				}
				if(close(socket_file_descriptor) == -1)
				{
					perror("sdfs_server:request_file_list - close");
					exit(1);
				}
				break;
			}
		}
	}
}

/**
 * Obtain the largest IP.\n
 * IP addresses are compared lexicographically.
 *
 * @param alive_IP a list of IP addresses from which the larged IP will be selected
 *
 * @return the largest IP address
 */
string get_highest_IP(vector<string> & alive_IP)
{
	string highest_IP = alive_IP[0];
	for(size_t i = 1; i < alive_IP.size(); i = i+1)
	{
		if(alive_IP[i] > highest_IP)
		{
			highest_IP = alive_IP[i];
		}
	}
	return highest_IP;
}

/**
 * Obtain a list of SDFS files that were stored in failed nodes and
 * update file list by removing IP's of the failed nodes
 *
 * @param lost_file a set that is to be filled with the SDFS files that were stored in failed nodes
 * @param alive_IP a list of alive IP(nodes)
 * @param file_list the mapping to IP and a list of SDFS files stored on that IP(node)
 */
void get_lost_file(set<string> & lost_file, vector<string> & alive_IP, map< string, vector<string> > & file_list)
{
	vector<string> dead_IP;

	/** Obtain a set of files that were stored in failed nodes */
	for(map< string, vector<string> >::iterator iter = file_list.begin(); iter != file_list.end(); iter++)
	{
		if(find(alive_IP.begin(), alive_IP.end(), iter->first) == alive_IP.end()) // "alive_IP" does not contain this IP from file list
		{
			for(size_t i = 0; i < iter->second.size(); i = i+1)
			{
				lost_file.insert(iter->second[i]);
			}

			dead_IP.push_back(iter->first);
		}
	}

	/** Update file list */
	for(size_t i = 0; i < dead_IP.size(); i = i+1)
	{
		file_list.erase(dead_IP[i]);
	}
}

/**
 * For SDFS files that are to be replicated, if the file exists on this node, the node fetches this file from its local SDFS and PUT's the file so that the cluster has the enough number of replicas
 *
 * @param lost_file a set of SDFS files to be replicated
 * @param local_file a list of files that exists in local SDFS
 */
void replicate(set<string> & lost_file, vector<string> & local_file)
{
	set<string> replicated_file; // used to update "lost_file" later

	/** do replication */
	for(set<string>::iterator iter = lost_file.begin(); iter != lost_file.end(); iter++)
	{
		if(find(local_file.begin(), local_file.end(), *iter) != local_file.end()) // this lost file exists in my SDFS
		{
			put(iter->c_str(), iter->c_str(), 0);
			replicated_file.insert(*iter);
		}
	}

	/** update "lost_file" */
	for(set<string>::iterator iter = replicated_file.begin(); iter != replicated_file.end(); iter++)
	{
		lost_file.erase(*iter);
	}
}

/**
 * Select a node with most lost file copies as the next replicator
 *
 * @param lost_file a set of files that were stored on failed nodes
 *
 * @param the IP address of the next replicator
 */
string get_next_replicator(set<string> & lost_file)
{
	map< string, vector<string> > candidate_pool;
	get_file_list(candidate_pool);
	int max = 0;
	string next_replicator;
	for(map< string, vector<string> >::iterator iter = candidate_pool.begin(); iter != candidate_pool.end(); iter++)
	{
		int num_file = 0;
		for(set<string>::iterator s = lost_file.begin(); s != lost_file.end(); s++)
		{
			if(find(iter->second.begin(), iter->second.end(), *s) != iter->second.end())
			{
				num_file = num_file + 1;
			}
		}
		if(num_file > max)
		{
			max = num_file;
			next_replicator = iter->first;
		}
	}

	return next_replicator;
}

/**
 * For SDFS files that are to be replicated, if the file exists on this node, the node fetches this file from its local SDFS and PUT's the file so that the cluster has the enough number of replicas.\n
 * If there are still lost files that are not stored on local machine, send lost-file list to the next replicator, who repeats the same process, until the lost-file list becomes empty
 * 
 * @param lost_file a set of SDFS files to be replicated
 * @param file_list a mapping of IP to a list of files hosted on this IP(mahine)
 * @param selfIP the IP address of this node
 */
void replicate(set<string> & lost_file, map< string, vector<string> > & file_list, string selfIP)
{
	/** Replicate the lost files that have replicas on this local machine */
	replicate(lost_file, file_list[selfIP]);

	/** Send "lost_file" to the node with the next highest IP, which will continue replicating the remaining lost files */
	if(!lost_file.empty()) // some lost file are still not replicated because I don't have copy of them on my site
	{
		string next_replicator = get_next_replicator(lost_file);
		int socket_file_descriptor;
		make_socket(socket_file_descriptor, next_replicator, REPLICATE_PORT);
		for(set<string>::iterator iter = lost_file.begin(); iter != lost_file.end(); iter++)
		{
			string message = *iter + "\n";
			if(send(socket_file_descriptor, message.c_str(), message.size(), 0) == -1)
			{
				perror("sdfs_server:replicate - send");
				exit(1);
			}
		}
		if(close(socket_file_descriptor) == -1)
		{
			perror("sdfs_server:replicate - close");
			exit(1);
		}
	}
}

/**
 * Detect node failure and replicate files whose replicas are stored in those filed nodes
 */
void check_failure()
{
	/** get file list and member list */
	map< string, vector<string> > file_list;
	get_file_list(file_list);
	vector<string> alive_IP;
	get_alive_IP(alive_IP);

	if(alive_IP.size() < file_list.size()) // failures detected
	{
		string highest_IP = get_highest_IP(alive_IP);
		string selfIP = get_selfIP();
		if(highest_IP.compare(selfIP) == 0) // I am the first replicator, do replication
		{
			sleep(6); // sleep for a while to let other VM finish updating their file_lists

			/** get a list of files that were hosted on failed VM's and remove failed VM's from in-memory file_list*/
			set<string> lost_file;
			get_lost_file(lost_file, alive_IP, file_list);

			/* update and boradcast local file_list */
			update_file_list(file_list);
			broadcast_file_list(file_list);

			replicate(lost_file, file_list, selfIP);
		}
		else
		{
			/* update local file list by removing failed VM's */
			set<string> lost_file;
			get_lost_file(lost_file, alive_IP, file_list);
			update_file_list(file_list);
		}
	}
}

/**
 * Receives replication request.
 * The host receives a list of lost files in SDFS and replicate those that exist in its local SDFS
 *
 * @param socket_file_descriptor the socket that receives the list of lost files
 */
void replicate_handler(int & socket_file_descriptor)
{
	struct sockaddr_storage their_addr; // connector's address information
	socklen_t sin_size = sizeof their_addr;
	int new_connection = accept(socket_file_descriptor, (struct sockaddr *)&their_addr, &sin_size);
	if(new_connection == -1)
	{
		perror("sdfs_server:replicate_handler - accept");
		exit(1);
	}
	if(!fork())
	{
		if(close(socket_file_descriptor) == -1) // child doesn't need the listener
		{
			perror("sdfs_server:replicate_handler - close listener in child");
			exit(1);
		}

		char buffer[BUFFER_LENGTH];
		memset(&buffer, 0, sizeof buffer);
		size_t num_recv;
		bool flag = 0;
		string raw = "";
		while((num_recv = recv(new_connection, buffer, BUFFER_LENGTH-1, 0)) > 0)
		{
			buffer[num_recv] = '\0';
			raw = raw + string(buffer);
			memset(&buffer, 0, sizeof buffer);
		}

		if(close(new_connection) == -1)
		{
			perror("sdfs_server:replicate_handler - close");
			exit(1);
		}

		/** Construct a set of lost files */
		set<string> lost_file;
		istringstream raw_stream(raw);
		string line;
		while(getline(raw_stream, line, '\n'))
		{
			lost_file.insert(line);
		}

		map< string, vector<string> > file_list;
		get_file_list(file_list);

		replicate(lost_file, file_list, get_selfIP());

		exit(0);
	}
	if(close(new_connection) == -1)  // parent doesn't need this
	{
		perror("close");
		exit(1);
	}
}

int main(void)
{
	/** Initialize sockets */
	int socket_put;
	int socket_get;
	int socket_delete;
	int socket_broadcast;
	int socket_join;
	int socket_replicate;
	make_socket(socket_put, PUT_PORT);
	make_socket(socket_get, GET_PORT);
	make_socket(socket_delete, DELETE_PORT);
	make_socket(socket_broadcast, BROADCAST_PORT);
	make_socket(socket_join, JOIN_PORT);
	make_socket(socket_replicate, REPLICATE_PORT);

	fd_set readfds; // socket set

	struct timeval timer; // select() timeout
	bool filelist_request_sent = 0;
	bool filelist_initialized = 0;
	while(1)
	{
		/** add all socket file descriptors to their corresponding socket sets */
		FD_ZERO(&readfds);
		FD_SET(socket_put, &readfds);
		FD_SET(socket_get, &readfds);
		FD_SET(socket_delete, &readfds);
		FD_SET(socket_broadcast, &readfds);
		FD_SET(socket_join, &readfds);
		FD_SET(socket_replicate, &readfds);

		/** set timeout period */
		timer.tv_sec = 3;
		timer.tv_usec = 0;

		/** select() */
		int numfds = find_max(socket_put, socket_get, socket_delete, socket_broadcast, socket_join, socket_replicate)+1;
		int num_ready = select(numfds, &readfds, NULL, NULL, &timer);
		if(num_ready == -1)
		{
			perror("select");
			exit(1);
		}
		if(num_ready)
		{
			if(FD_ISSET(socket_put, &readfds))
			{
				load(socket_put);
			}
			if(FD_ISSET(socket_get, &readfds))
			{
				pull(socket_get);
			}
			if(FD_ISSET(socket_delete, &readfds))
			{
				remove(socket_delete);
			}
			if(FD_ISSET(socket_broadcast, &readfds))
			{
				replace_file_list(socket_broadcast, filelist_initialized);
			}
			if(FD_ISSET(socket_join, &readfds))
			{
				onboard(socket_join);
			}
			if(FD_ISSET(socket_replicate, &readfds))
			{
				replicate_handler(socket_replicate);
			}
		}
		if(!filelist_request_sent)
		{
			request_file_list(filelist_initialized);
			filelist_request_sent = 1;
		}
		else
		{
			check_failure(); // handle failure
		}
	}

	return 0;
}
