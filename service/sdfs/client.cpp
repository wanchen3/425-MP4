/**
 * @file client.cpp
 *
 * SDFS client
 *
 * @author Jiaqi Liu
 */

#include "share.h"
#include <dirent.h>
#include <algorithm>

/**
 * Validate and parse arguments to get operation to be performed
 *
 * @param n_argument the number of arguments
 * @param arguments an array of arguments
 *
 * @return the operation to be performed(PUT, GET, DELETE, store, list)
 */
string argument_check(int n_argument, char *arguments[])
{
	if(n_argument != 4 && n_argument != 3 && n_argument != 2)
	{
		fprintf(stderr,"usage:\n\t./sdfs put local_filename sdfs_filename\n\t./sdfs get sdfs_filename local_filename\n\t./sdfs delete sdfs_filename\n\t./sdfs list sdfs_filename\n\t./sdfs store\n");
		exit(1);
	}
	else
	{
		string operation(arguments[1]);
		if(n_argument == 4) // perform put or get operations
		{
			if(operation != "put" && operation != "get")
			{
				fprintf(stderr,"usage:\n\t./sdfs put local_filename sdfs_filename\n\t./sdfs get sdfs_filename local_filename\n\t./sdfs delete sdfs_filename\n\t./sdfs list sdfs_filename\n\t./sdfs store\n");
				exit(1);
			}
			else
			{
				return operation;
			}
		}
		else if(n_argument == 3) // perform delete or list operations
		{
			if(operation != "delete" && operation != "list")
			{
				fprintf(stderr,"usage:\n\t./sdfs put local_filename sdfs_filename\n\t./sdfs get sdfs_filename local_filename\n\t./sdfs delete sdfs_filename\n\t./sdfs list sdfs_filename\n\t./sdfs store\n");
				exit(1);
			}
			else if(operation == "list")
			{
				return operation;
			}
			else if(operation == "delete")
			{
			        return operation;
			}
			else
			{
			        return operation;
			}
		}
		else // perform store operation 
		{
			if(operation != "store")
			{
				fprintf(stderr,"usage:\n\t./sdfs put local_filename sdfs_filename\n\t./sdfs get sdfs_filename local_filename\n\t./sdfs delete sdfs_filename\n\t./sdfs list sdfs_filename\n\t./sdfs store\n");
				exit(1);
			}
			else
			{
				return operation;
			}
		}
	}
}

/**
 * Use a socket to request a file from remote server and store the requested file content in local file system
 *
 * @param socket_file_descriptor the socket between local machine the the remote server that hosts the requested file
 * @param sdfs_filename the name of the file that is to be fetched from the remote server
 * @param local_filename the new name of the file in local file system
 */
void get_file(int & socket_file_descriptor, char * sdfs_filename, const char * local_filename)
{
	/** send sdfs_filename so that server knows the name of file in file system */
	string filename = sdfs_filename;
	if(send(socket_file_descriptor, filename.c_str(), (int)(filename.size()), 0) == -1)
	{
		perror("send");
		exit(1);
	}

	/** read data from server and put it into "local_filename" */
	FILE * file_get;
	file_get = fopen(local_filename, "wb");
	if(file_get == NULL)
	{
		fputs("File error", stderr);
		exit(1);
	}

	char buffer[BUFFER_LENGTH];
	memset(&buffer, 0, sizeof buffer); 

	int received;
	while( (received = recv(socket_file_descriptor, buffer, BUFFER_LENGTH-1, 0)) > 0 )
	{
		buffer[received] = '\0';
		string content = buffer; // convert C string to C++ string for easier string manipulation
		fwrite(buffer, sizeof(char), received, file_get);                    
	}
}

/**
 * GET a file from SDFS and store it in local file system
 *
 * @param sdfs_filename name of the file in SDFS
 * @param local_filename new name of the file in local file system
 */
void get(char * sdfs_filename, char * local_filename)
{
	/** get local file list */
	map< string, vector<string> > file_list;
	get_file_list(file_list);

	/** Handle "file does not exist" case */
	bool file_exist = 0;
	string ip_alive = "";
	for(map< string, vector<string> >::iterator iter = file_list.begin(); iter != file_list.end(); iter++)
	{
		if(find(iter->second.begin(), iter->second.end(), string(sdfs_filename)) != iter->second.end())
		{
			file_exist = 1;
			ip_alive = iter->first;
			break;
		}
	}
	if(!file_exist)
	{
		cout << '"' << string(sdfs_filename) << '"' << " does not exist in SDFS" << endl;
		return;
	}

	/** connect with 1 node */
	int socket_file_descriptor;
	make_socket(socket_file_descriptor, ip_alive, GET_PORT);

	/** get file */
	string filename = "local_file/";
	string str(local_filename);
	filename = filename + str;
	get_file(socket_file_descriptor, sdfs_filename, filename.c_str());

	if(close(socket_file_descriptor) == -1)
	{
		perror("close");
		exit(1);
	}
}

/**
 * Given a SDFS file name as a delete target, obtain a list of nodes(IP addresses) that host the file. 
 * In-memory file list is also updated by removing the file name under those nodes
 *
 * @param delete_target a list of IP addresses to which DELETE request is sent
 * @param file_list a mapping of IP address to a list of files hosted on the IP(machine)
 * @param sdfs_filename the name of the file to be deleted in SDFS
 */
void get_delete_target(vector<string> & delete_target, map< string, vector<string> > & file_list, char * sdfs_filename)
{
	for(map< string, vector<string> >::iterator iter = file_list.begin(); iter != file_list.end(); iter++)
	{
		for(vector<string>::iterator viter = iter->second.begin(); viter != iter->second.end(); viter++)
		{
			if(viter->compare(string(sdfs_filename)) == 0)
			{
				delete_target.push_back(iter->first);
				break;
			}
		}
	}

	/** Update file list */
	for(size_t i = 0; i < delete_target.size(); i = i+1)
	{
		file_list[ delete_target[i] ].erase( find(file_list[ delete_target[i] ].begin(), file_list[ delete_target[i] ].end(), string(sdfs_filename)) );
	}
}

/**
 * Delete a file from SDFS
 *
 * @param sdfs_filename the name of the file to be deleted in SDFS
 */
void remove(char * sdfs_filename)
{
	/** get local file list */
	map< string, vector<string> > file_list;
	get_file_list(file_list);

	/** get a list of VM's that host "sdfs_filename" */
	vector<string> delete_target;
	get_delete_target(delete_target, file_list, sdfs_filename);
	if(delete_target.empty()) // file to be deleted does not exist in SDFS
	{
		cout << '"' << string(sdfs_filename) << '"' << " does not exist in SDFS" << endl;
		exit(0);
	}

	/** send DELETE request to corresponding VM's */
	for(size_t i = 0; i < delete_target.size(); i = i+1)
	{
		int socket_file_descriptor;
		make_socket(socket_file_descriptor, delete_target[i], DELETE_PORT);
		if(send(socket_file_descriptor, sdfs_filename, sizeof(char)*strlen(sdfs_filename), 0) == -1)
		{
			perror("send");
			exit(1);
		}
	}

	/** update and broadcast file list */
	update_file_list(file_list);
	broadcast_file_list(file_list);
}

/**
 * List SDFS files stored on this machine
 *
 * @author <a href = "https://www.linkedin.com/in/yongli-chen-b3ab853a">Yongli Chen</a>
 */
void listsdfsdirfilename(void)
{
	DIR *dpdf;
	struct dirent *epdf;
	dpdf = opendir("./sdfs_file/");
	if (dpdf != NULL)
	{
		while (epdf = readdir(dpdf))
		{
			printf("Filename: %s\n",epdf->d_name);
		}
	}
}

/**
 * List the IP addresses of nodes that store a particular file in SDFS
 *
 * @param filename the name of the file to be searched in SDFS
 *
 * @author <a href = "https://www.linkedin.com/in/yongli-chen-b3ab853a">Yongli Chen</a>
 */
void listfilelocation(char * filename)
{
	/* get local file list */
	map< string, vector<string> > file_list;
	get_file_list(file_list);

	int hit = 0;
	string ip_alive;
	vector<string> fl;
	for(map<string, vector<string> >::iterator it=file_list.begin(); it!=file_list.end(); it++)
	{  
		ip_alive = it->first;
		fl = it->second;
		if(find(fl.begin(), fl.end(), filename) != fl.end())
		{
			cout<<endl<<ip_alive<<endl;
			hit++;
		}
	}

	if(!hit) //didn't find file in sdfs
	cout<<"No such file found in sdfs\n"<<endl;
}

int main(int n_argument, char *arguments[])
{
	string operation = argument_check(n_argument, arguments);

	if(operation == "put")
	{
		put(arguments[2], arguments[3], 1); // put operation
	}
	else if(operation == "get")
	{
		get(arguments[2], arguments[3]); // get operation
	}
	else if(operation == "delete")
	{
		remove(arguments[2]); // delete operation
	}
	else if(operation == "store")
	{
		listsdfsdirfilename(); // list all files stored in sdfs_file directory
	}
	else if(operation == "list")
	{
		listfilelocation(arguments[2]);
	}

	return 0;
}
