#!/bin/bash

awk '{print $1}' $1 > 1.data
awk '{print $2}' $1 > 2.data
paste 2.data 1.data > $2
rm 1.data
rm 2.data

