#!/bin/bash

log()
{
	date | cut -d " " -f 1-5,7 | tr -d '\n' >> ./log/master.log
	printf " - $1\n" >> ./log/master.log
}

print_usage()
{
	printf "Usage: $0 [--executable=<string>] [--number_of_workers=<int>] [--output=<string>] [--input=<string>] [--delete_input=<0 or 1>]\n"
	exit 1
}

get_alive_workers()
{
	unset alive_workers # clear contents of array
	selfIP=$(<../service/logger/selfIP.txt)
	while read IP; do
		if [ $selfIP != $IP ]; then
				alive_workers+=( $IP )
		fi
	done <../service/logger/member.txt
}

# -------------- #
# argument check #
# -------------- # 
for argument in "$@"; do
	case $argument in
		--executable=*)
			executable=$(cut -d "=" -f 2 <<< $argument)
			shift
			;;
		--number_of_workers=*)
			number_of_workers=$(cut -d "=" -f 2 <<< $argument)
			shift
			;;
		--output=*)
			output=$(cut -d "=" -f 2 <<< $argument)
			shift
			;;
		--input=*)
			input=$(cut -d "=" -f 2 <<< $argument)
			shift
			;;
		--delete_input=*)
			delete_input=$(cut -d "=" -f 2 <<< $argument)
			shift
			;;
		*)
			print_usage
			;;
	esac
done

if [ -z "${executable}" ] || [ -z "${number_of_workers}" ] || [ -z "${output}" ] || [ -z "${input}" ]; then
	print_usage
fi

# ------------------- #
# validate executable #
# ------------------- #
if [ $executable != "word_count" ] && [ $executable != "reverse" ]; then
	printf "$executable program not found - available programs are word_count and reverse\n"
	exit 1
fi

# ---------------------------------------------------------------- #
# make sure user requests no more than number of available workers #
# ---------------------------------------------------------------- #
alive_workers=()
selfIP=""
get_alive_workers
if [ "${#alive_workers[@]}" -lt "$number_of_workers" ]; then
	printf "You requested more than the number of of available workers. There are ${#alive_workers[@]} workers.\n"
	exit 1;
fi

# ------------- #
# setup logging #
# ------------- #
if [ ! -d ./log ]; then
	mkdir log
fi

printf "==================================================\n" >> ./log/master.log
log "$executable application starts"
log "$number_of_workers workers"
log "Input prefix is $input"
log "Output is $output"


# ------------------------------------------------------- #
# GET file from SDFS and partition it into multiple files #
# ------------------------------------------------------- #
inputs=()
while read file; do
	inputs+=( $file )
done <./juice.input

if [ ! -d ./input ]; then
    mkdir input
fi

cd ../service/sdfs
for input in "${inputs[@]}"; do
	./sdfs get $input $input
	cp ./local_file/$input ../../batch_processing/input
done

cd ../../batch_processing/input
if [ $executable == "word_count" ]; then
	cat * | awk '{print $2,$1}' | sort |  awk '{arr[$1]+=$2;} END {for (i in arr) print i, arr[i]}' > $output
fi
if [ $executable == "reverse" ]; then
	cat * > $output
fi

#
#cd ../../batch_processing/input
## sort - the first key is sorted numerically in reverse order while the second is sorted as per the default sort order
## awk - split file according to value in the first col
#cat * | sort -k1,1nr -k2,2 | awk '{print >> $1; close($1)}'
#partitions=()
#for partition in *; do
#	partitions+=( $partition )
#done
#cd ../
#
## ---------------------------------------------- #
## assign jobs to and start jobs in alive workers #
## ---------------------------------------------- #
#if [ $executable == "word_count" ]; then
#	for partition in "${partitions[@]}"; do
#		./word_count/juice_word_count.sh >> $output
#	done
#fi
#
## -------- #
## clean up #
## -------- #
#if [ $delete_input -eq 1 ]; then
#	cd input
#	rm *
#fi
