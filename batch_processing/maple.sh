#!/bin/bash

log()
{
	date | cut -d " " -f 1-5,7 | tr -d '\n' >> ./log/master.log
	printf " - $1\n" >> ./log/master.log
}

print_usage()
{
	printf "Usage: $0 [--executable=<string>] [--number_of_workers=<int>] [--output=<string>] [--input=<string>]\n"
	exit 1
}

get_alive_workers()
{
	alive_workers=() # clear contents of array
	selfIP=$(<../service/logger/selfIP.txt)
	while read IP; do
		if [ $selfIP != $IP ]; then
				alive_workers+=( $IP )
		fi
	done <../service/logger/member.txt
}

# -------------- #
# argument check #
# -------------- # 
for argument in "$@"; do
	case $argument in
		--executable=*)
			executable=$(cut -d "=" -f 2 <<< $argument)
			shift
			;;
		--number_of_workers=*)
			number_of_workers=$(cut -d "=" -f 2 <<< $argument)
			shift
			;;
		--output=*)
			output=$(cut -d "=" -f 2 <<< $argument)
			shift
			;;
		--input=*)
			input=$(cut -d "=" -f 2 <<< $argument)
			shift
			;;
		*)
			print_usage
			;;
	esac
done

if [ -z "${executable}" ] || [ -z "${number_of_workers}" ] || [ -z "${output}" ] || [ -z "${input}" ]; then
	print_usage
fi

# ------------------- #
# validate executable #
# ------------------- #
if [ $executable != "word_count" ] && [ $executable != "reverse" ]; then
	printf "$executable program not found - available programs are word_count and reverse\n"
	exit 1
fi

# ---------------------------------------------------------------- #
# make sure user requests no more than number of available workers #
# ---------------------------------------------------------------- #
alive_workers=()
selfIP=""
get_alive_workers
if [ "${#alive_workers[@]}" -lt "$number_of_workers" ]; then
	printf "You requested more than the number of of available workers. There are ${#alive_workers[@]} workers.\n"
	exit 1;
fi

# ------------- #
# setup logging #
# ------------- #
if [ ! -d ./log ]; then
	mkdir log
	touch ./log/master.log
fi

> ./log/master.log # clear log
log "$executable application starts"
log "$number_of_workers workers"
log "Input is $input"
log "Maple output prefix is $output"

# ------------------------------------------------------------------------ #
# GET file from SDFS and partition it based on number of requested workers #
# ------------------------------------------------------------------------ #
if [ ! -d ./input ]; then
    mkdir input
	log "Maple input directory created"
fi

if [ $executable == "word_count" ]; then
	cp word_count/word_count_input.data ../service/sdfs/word_count_input.data
fi
cd ../service/sdfs
./sdfs get $input $input
cp ./local_file/$input ../../batch_processing/input
cd ../../batch_processing/input
split -l$((`wc -l < $input`/$(($number_of_workers-1)))) $input $input
rm $input
partitions=()
for partition in *; do
	partitions+=( $partition )
done
cd ../

# ----------------------------------------------------------------------------------- #
# Start nc server                                                                     #
# When a worker finishes job on a data partition, it contacts this master's nc server #
# so that the master marks the partition as finished                                  #
# ----------------------------------------------------------------------------------- #
nc -l -k 5555 >> ./log/master.log &

# ---------------------------------------------- #
# assign jobs to and start jobs in alive workers #
# ---------------------------------------------- #
declare -A fileToIP # key = one file partition, value = IP of worker that process this file partition
> juice.input
for i in "${!partitions[@]}"; do
	partition=${partitions[$i]}
	worker=${alive_workers[$i]}
	fileToIP[$partition]=$worker
	log "Assign $partition to ${fileToIP[$partition]}"
	ssh $worker "rm -rf batch_processing && mkdir batch_processing" < /dev/null
	if [ $executable == "word_count" ]; then
		scp word_count/maple_word_count.sh input/$partition $worker:~/batch_processing
		ssh -f $worker "cd batch_processing && ./maple_word_count.sh $partition ../service/sdfs/local_file/$output$i && cd ../service/sdfs && sleep 6 && ./sdfs put $output$i $output$i && (printf \"$worker done\n\" | nc $selfIP 5555)"
	fi

	if [ $executable == "reverse" ]; then
		scp reverse/maple_reverse.sh input/$partition $worker:~/batch_processing
		ssh -f $worker "cd batch_processing && ./maple_reverse.sh $partition ../service/sdfs/local_file/$output$i && cd ../service/sdfs && sleep 6 && ./sdfs put $output$i $output$i && (printf \"$worker done\n\" | nc $selfIP 5555)"
	fi

	printf "$output$i\n" >> juice.input
done

# ---------------------------------------------------- #
# detect node failures and re-assign job to alive node #
# ---------------------------------------------------- #
RANDOM=$$$(date +%s)
while [ $(cat ./log/master.log | grep done | wc -l) -lt $number_of_workers ]; do
	get_alive_workers

	if [ "${#alive_workers[@]}" -eq 0 ]; then
		log "all workers failed. Abort application"
		exit 1;
	fi	

	for i in "${!fileToIP[@]}"; do
		failed=1
		for alive_worker in "${alive_workers[@]}"; do
			if [ "$alive_worker" == "${fileToIP[$i]}" ]; then
				failed=0
				break;
			fi
		done
		if [ $failed -eq 1 ]; then
			log "${fileToIP[$i]} failed"
			unset fileToIP[$i]

			worker=${alive_workers[$RANDOM % ${#alive_workers[@]} ]}
			partition=$i
			fileToIP[$partition]=$worker
			log "Re-assign $partition to ${fileToIP[$partition]}"

			ssh $worker "rm -rf batch_processing && mkdir batch_processing" < /dev/null
			if [ $executable == "word_count" ]; then
				scp -r word_count/maple_word_count.sh input/$partition $worker:~/batch_processing
				ssh -f $worker "cd batch_processing && ./maple_word_count.sh $partition ../service/sdfs/local_file/$output$i && cd ../service/sdfs && sleep 6 && ./sdfs put $output$i $output$i && printf \"$worker done\n\" | nc $selfIP 5555"
			fi
			if [ $executable == "reverse" ]; then
				scp reverse/maple_reverse.sh input/$partition $worker:~/batch_processing
				ssh -f $worker "cd batch_processing && ./maple_reverse.sh $partition ../service/sdfs/local_file/$output$i && cd ../service/sdfs && sleep 6 && ./sdfs put $output$i $output$i && (printf \"$worker done\n\" | nc $selfIP 5555)"
			fi
		fi
	done
done

log "$executable application finishes"

# -------- #
# clean up #
# -------- #
kill $(ps | grep "nc" | awk '{print $1}')
rm ./input/*
#cd ../service/sdfs
#rm local_file/$input
#./sdfs delete $input
