/**
 * @file membership_server.cpp
 *
 * Server that receives pings on each node.
 * If this node is still up, the client that pings this server will mark this node as alive, otherwise, the client marks this server as failed in the client's membership list
 *
 * @author Jiaqi Liu
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>

#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>

/**
 * @def PORT
 * @brief The port clients will be connecting to
 *
 * The membership server(server.cpp) listens on this port. The ping messages are sent to this port
 */
#define PORT "3490"

/**
 * @def BACKLOG
 * @brief Max number of pending connections
 */
#define BACKLOG 30

/**
 * Reap all dead processes
 */
void sigchld_handler(int s)
{
	// waitpid() might overwrite errno, so we save and restore it:
	int saved_errno = errno;

	while(waitpid(-1, NULL, WNOHANG) > 0);

	errno = saved_errno;
}

/**
 * Get sockaddr - IPv4 or IPv6 
 *
 * @param socket_address
 */
void *get_in_addr(struct sockaddr *socket_address)
{
	if (socket_address->sa_family == AF_INET)
	{
		return &( ((struct sockaddr_in*)socket_address)->sin_addr );
	}
	else
	{
		return &( ((struct sockaddr_in6*)socket_address)->sin6_addr );
	}
}

int main(void)
{
	struct addrinfo hints;
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;

	int result;
	struct addrinfo *server_information;
	if( (result = getaddrinfo(NULL, PORT, &hints, &server_information)) != 0 )
	{
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(result));
		return 1;
	}

	struct addrinfo *temp;
	int yes=1;
	// loop through all the results and bind to the first we can
	int socket_descriptor;
	for(temp = server_information; temp != NULL; temp = temp->ai_next)
	{
		if( (socket_descriptor = socket(temp->ai_family, temp->ai_socktype, temp->ai_protocol)) == -1)
		{
			perror("server: socket");
			continue;
		}

		if( setsockopt(socket_descriptor, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1 )
		{
			perror("setsockopt");
			exit(1);
		}

		if(bind(socket_descriptor, temp->ai_addr, temp->ai_addrlen) == -1)
		{
			close(socket_descriptor);
			perror("server: bind");
			continue;
		}

		break;
	}

	if (temp == NULL)
	{
		fprintf(stderr, "server: failed to bind\n");
		return 2;
	}

	freeaddrinfo(server_information); // all done with this structure

	// Tell a socket to listen for incoming connections.
	if (listen(socket_descriptor, BACKLOG) == -1)
	{
		perror("listen");
		exit(1);
	}

	struct sigaction sa;
	sa.sa_handler = sigchld_handler;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = SA_RESTART;
	if (sigaction(SIGCHLD, &sa, NULL) == -1)
	{
		perror("sigaction");
		exit(1);
	}

	//printf("server: waiting for connections...\n");

	while(1)
	{
		struct sockaddr_storage their_addr; // connector's address information
		socklen_t sin_size = sizeof their_addr;
		int new_connection = accept(socket_descriptor, (struct sockaddr *)&their_addr, &sin_size);
		if(new_connection == -1)
		{
			perror("accept");
			continue;
		}

		close(new_connection);  // parent doesn't need this
	}

	return 0;
}
