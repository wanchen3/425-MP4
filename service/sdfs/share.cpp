/**
 * @file share.cpp
 *
 * @author Jiaqi Liu
 */

#include "share.h"

void put(const char * local_filename, const char * sdfs_filename, bool from_local)
{
	/**
	 * Get local file list
	 */
	map< string, vector<string> > file_list;
	get_file_list(file_list);
	
	/**
	 * Compute the node list that receives PUT request to this file("sdfs_filename")
	 */
	vector<string> put_target;
	get_put_target(put_target, file_list, sdfs_filename);

	/**
	 * send file to target VM's
	 */
	for(size_t i = 0; i < put_target.size(); i = i+1)
	{
		int socket_file_descriptor;
		make_socket(socket_file_descriptor, put_target[i], PUT_PORT);
		send_file(socket_file_descriptor, local_filename, sdfs_filename, from_local);
		if(close(socket_file_descriptor) == -1)
		{
			perror("close");
			exit(1);
		}

		file_list[ put_target[i] ].push_back( string(sdfs_filename) ); // needed for updating file list later
	}

	/**
	 * update and broadcast file list
	 */
	update_file_list(file_list);
	broadcast_file_list(file_list);
}

void get_file_list(map< string, vector<string> > & file_list)
{
	/**
	 * get file list content
	 */
	string raw;
	read_file_list(raw); // put file content in "raw"

	/**
	 * Deserialize file list to a dictionary. Key = IP(machine), value = file names on that IP
	 */
	istringstream raw_stream(raw);
	string line;
	while(getline(raw_stream, line, '\n'))
	{
		istringstream temp(line);
		string IP;
		getline(temp, IP, ' ');
		vector<string> files;
		while(getline(temp, line, ' '))
		{
			files.push_back(line);
		}
		file_list[IP] = files;
	}
}

void read_file_list(string & raw)
{
	raw = "";

	/**
	 * Set up read lock
	 */
	struct flock file_lock;
	file_lock.l_type = F_RDLCK;
	file_lock.l_whence = SEEK_SET;
	file_lock.l_start = 0;
	file_lock.l_len = 0;
	file_lock.l_pid = getpid();

	/**
	 * Open input file with lock
	 */
	int input;
	if((input = open("../logger/file_list.txt", O_RDONLY)) == -1)
	{
		perror("open");
		exit(1);
	}
	if(fcntl(input, F_SETLKW, &file_lock) == -1)
	{
		perror("fcntl");
		exit(1);
	}

	/**
	 * Put file content into a C++ string(easier to manipulate)
	 */
	int buffer_size = 18;
	char buffer[buffer_size];
	memset(&buffer, 0, sizeof buffer);
	ssize_t num_read;
	while(1)
	{
		num_read = read(input, buffer, buffer_size);
		if(num_read == -1)
		{
			perror("read");
			exit(1);
		}
		if(num_read > 0)
		{
			buffer[num_read] = '\0';
			raw = raw + string(buffer);
			memset(&buffer, 0, sizeof buffer);
		}
		else
		{
			break;
		}
	}

	/**
	 * Unlock and close input file
	 */
	file_lock.l_type = F_UNLCK;
	if(fcntl(input, F_SETLK, &file_lock) == -1)
	{
		perror("fcntl");
		exit(1);
	}
	if(close(input) == -1)
	{
		perror("close");
		exit(1);
	}
}

void get_put_target(vector<string> & put_target, map< string, vector<string> > & file_list, const char * sdfs_filename)
{
	/**
	 * get current number of replicas of sdfs_filename in cluster
	 */
	int curr_num_replica = 0;
	vector<string> target_candidate;
	for(map< string, vector<string> >::iterator iter = file_list.begin(); iter != file_list.end(); iter++)
	{
		bool is_candidate = 1;
		for(size_t i = 0; i < iter->second.size(); i = i+1)
		{
			if(iter->second[i].compare(string(sdfs_filename)) == 0)
			{
				curr_num_replica = curr_num_replica+1;
				is_candidate = 0; // this VM already has the file, don't send the file to it
				break;
			}
		}
		if(is_candidate)
		{
			inorder_insert(target_candidate, file_list, iter->first, iter->second.size());
		}
	}

	/**
	 * Check number of current replicas in cluster.
	 * If it's not enough, pick up the IP's that have the leases amount of files stored on them, then send file that is to be replicated to them
	 */
	if(curr_num_replica < NUM_REPLICA)
	{
		for(size_t i = 0; i < target_candidate.size(); i = i+1)
		{
			put_target.push_back(target_candidate[i]);
			curr_num_replica = curr_num_replica + 1;
			if(curr_num_replica == NUM_REPLICA)
			{
				break;
			}
		}
	}
	else
	{
		return;
	}
}

void inorder_insert(vector<string> & target_candidate, map< string, vector<string> > & file_list, string key, size_t value)
{
	if(target_candidate.empty())
	{
		target_candidate.push_back(key);
	}
	else
	{
		if(value < file_list[ target_candidate[target_candidate.size()-1] ].size())
		{
			string temp = target_candidate[target_candidate.size()-1];
			target_candidate.pop_back();
			inorder_insert(target_candidate, file_list, key, value);
			target_candidate.push_back(temp);
		}
		else
		{
			target_candidate.push_back(key);
		}
	}
}

void make_socket(int & socket_file_descriptor, string & IP, const char * port)
{
	struct addrinfo hints;
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;

	int result;
	struct addrinfo *server_information;
	if((result = getaddrinfo(IP.c_str(), port, &hints, &server_information)) != 0)
	{
		//cout << "share make_socket: " << IP.c_str() << ", " << port << endl;
		fprintf(stderr, "getaddrinfo(share.cpp make_socket()): %s\n", gai_strerror(result));
		exit(1);
	}

	/**
	 * Loop through all the results and connect to the first we can
	 */
	struct addrinfo * temp;
	for(temp = server_information; temp != NULL; temp = temp->ai_next)
	{
		/**
		 * get socket descriptor
		 */
		if((socket_file_descriptor = socket(temp->ai_family, temp->ai_socktype, temp->ai_protocol)) == -1)
		{
			perror("client: socket");
			continue;
		}

		/**
		 * connect socket to server
		 */
		if(connect(socket_file_descriptor, temp->ai_addr, temp->ai_addrlen) == -1)
		{
			close(socket_file_descriptor);
			perror("client: connect");
			continue;
		}

		break;
	}

	if(temp == NULL)
	{
		string message = get_selfIP() + " - share:make_socket - client: failed to connect\n";
		fprintf(stderr, message.c_str());
		exit(2);
	}

	freeaddrinfo(server_information); // all done with this structure
}

void send_file(int & socket_file_descriptor, const char * local_filename, const char * sdfs_filename, bool from_local)
{
	/**
	 * send sdfs_filename so that server knows the name of file in file system
	 */
	string filename = sdfs_filename;
	filename = filename + ":";
	if(send(socket_file_descriptor, filename.c_str(), (int)(filename.size()), 0) == -1)
	{
		perror("send");
		exit(1);
	}

	/**
	 * send file content
	 */
	FILE * file_put;
	string directory;
	if(from_local)
	{
		directory = "./local_file/"; // send file in local_file directory
	}
	else
	{
		directory = "./sdfs_file/"; // send file in sdfs_file directory
	}
	if((file_put = fopen((directory+string(local_filename)).c_str(), "rb")) == NULL)
	{
		perror("fopen");
		exit(1);
	}
	else
	{
		char buffer[BUFFER_LENGTH];
		memset(&buffer, 0, sizeof buffer);
		int read;
		while( (read = fread(buffer, sizeof(char), BUFFER_LENGTH, file_put)) > 0)
		{
			if(read < BUFFER_LENGTH)
			{
				if(send(socket_file_descriptor, buffer, read, 0) == -1)
				{
					perror("send");
				}
				break;
			}
			else
			{
				if(send(socket_file_descriptor, buffer, BUFFER_LENGTH, 0) == -1)
				{
					perror("send");
				}
			}
		}
		if(fclose(file_put) == EOF)
		{
			perror("fclose");
			exit(1);
		}
	}
}

void update_file_list(map< string, vector<string> > & file_list)
{
	/** setup write-lock */
	struct flock file_lock;
	file_lock.l_type = F_WRLCK;
	file_lock.l_whence = SEEK_SET;
	file_lock.l_start = 0;
	file_lock.l_len = 0;
	file_lock.l_pid = getpid();

	/** acquire lock and open file */
	int input;
	if((input = open("../logger/file_list.txt", O_WRONLY | O_TRUNC)) == -1)
	{
		perror("open");
		exit(1);
	}
	if(fcntl(input, F_SETLKW, &file_lock) == -1)
	{
		perror("fcntl");
		exit(1);
	}

	/** flush new file list to disk */
	for(map< string, vector<string> >::iterator iter = file_list.begin(); iter != file_list.end(); iter++)
	{
		string line = iter->first;
		for(size_t i = 0; i < iter->second.size(); i = i+1)
		{
			line = line + " " + iter->second[i];
		}
		line = line + "\n";
		write(input, line.c_str(), line.size());
	}

	/** close file and release lock */
	file_lock.l_type = F_UNLCK;
	if(fcntl(input, F_SETLK, &file_lock) == -1)
	{
		perror("fcntl");
		exit(1);
	}
	if(close(input) == -1)
	{
		perror("close");
		exit(1);
	}
}

void broadcast_file_list(map< string, vector<string> > & file_list)
{
	for(map< string, vector<string> >::iterator iter = file_list.begin(); iter != file_list.end(); iter ++)
	{
		int socket_file_descriptor;
		string IP = iter->first;
		make_socket(socket_file_descriptor, IP, BROADCAST_PORT);
		send_file_list(socket_file_descriptor);
		if(close(socket_file_descriptor) == -1)
		{
			perror("close");
			exit(1);
		}
	}	
}

void send_file_list(int & socket_file_descriptor)
{
	/** get file content */
	string raw;
	read_file_list(raw); // put file content in "raw"

	/** send file */
	if(send(socket_file_descriptor, raw.c_str(), raw.size(), 0) == -1)
	{
		perror("send");
		exit(1);
	}
}

string get_selfIP()
{
	ifstream input("../logger/selfIP.txt");
	if(input.is_open())
	{
		string selfIP;
		getline(input, selfIP, '\n');
		input.close();
		return selfIP;
	}
	else
	{
		cout << "share: get_selfIP - cannot open \"selfIP.txt\"" << endl;
		exit(1);
	}
}

void print_file_list(map< string, vector<string> > & file_list, string identifier)
{
	cout << identifier << ":" << endl;
	for(map< string, vector<string> >::iterator iter = file_list.begin(); iter != file_list.end(); iter++)
	{
		cout << iter->first << ":";
		for(size_t i = 0; i < iter->second.size(); i = i+1)
		{
			cout << " " << iter->second[i];
		}
		cout << endl;
	}
	cout << "============================================================" << endl;
}
