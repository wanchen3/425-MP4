/**
 * @file server.c
 *
 * HTTP file server
 *
 * @author Jiaqi Liu
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>

#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>

/**
 * @def BACKLOG
 * @brief The max number of pending connections
 */
#define BACKLOG 30

/**
 * @def REQUEST_LENGTH
 * @brief The length of requeste file path
 */
#define REQUEST_LENGTH 600

/**
 * @def BUFFER_SEND
 * @brief The size of buffer used to send requestded data
 */
#define BUFFER_SEND 600000

void sigchld_handler(int s)
{
	while(waitpid(-1, NULL, WNOHANG) > 0);
}

/**
 * Get sockaddr - IPv4 or IPv6 
 *
 * @param socket_address
 */
void *get_in_addr(struct sockaddr *socket_address)
{
	if (socket_address->sa_family == AF_INET)
	{
		return &( ((struct sockaddr_in*)socket_address)->sin_addr );
	}

	return &( ((struct sockaddr_in6*)socket_address)->sin6_addr );
}

int main(int argument_number, char *argument_vector[])
{
	int yes=1;

	if(argument_number != 2)
	{
	    fprintf(stderr,"usage: specify a port number as the argument\n");
	    exit(1);
	}

	struct addrinfo hints;
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE; // use my IP

	int result;
	struct addrinfo *server_information;
	if( (result = getaddrinfo(NULL, argument_vector[1], &hints, &server_information)) != 0 )
	{
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(result));
		return 1;
	}

	struct addrinfo *temp;
	// loop through all the results and bind to the first we can
	int socket_descriptor;
	for(temp = server_information; temp != NULL; temp = temp->ai_next)
	{
		if( (socket_descriptor = socket(temp->ai_family, temp->ai_socktype, temp->ai_protocol)) == -1)
		{
			perror("server: socket");
			continue;
		}

		if( setsockopt(socket_descriptor, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1 )
		{
			perror("setsockopt");
			exit(1);
		}

		if(bind(socket_descriptor, temp->ai_addr, temp->ai_addrlen) == -1)
		{
			close(socket_descriptor);
			perror("server: bind");
			continue;
		}

		break;
	}

	if (temp == NULL)
	{
		fprintf(stderr, "server: failed to bind\n");
		return 2;
	}

	freeaddrinfo(server_information); // all done with this structure

	// Tell a socket to listen for incoming connections.
	if (listen(socket_descriptor, BACKLOG) == -1)
	{
		perror("listen");
		exit(1);
	}

	struct sigaction sa;
	sa.sa_handler = sigchld_handler; // reap all dead processes
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = SA_RESTART;
	if (sigaction(SIGCHLD, &sa, NULL) == -1)
	{
		perror("sigaction");
		exit(1);
	}

	//printf("server: waiting for connections...\n");

	while(1)
	{
		struct sockaddr_storage their_addr; // connector's address information
		socklen_t sin_size = sizeof their_addr;
		int new_connection = accept(socket_descriptor, (struct sockaddr *)&their_addr, &sin_size);
		if(new_connection == -1)
		{
			perror("accept");
			continue;
		}

		char ip_read[INET6_ADDRSTRLEN];
		inet_ntop(their_addr.ss_family, get_in_addr((struct sockaddr *)&their_addr), ip_read, sizeof ip_read);
		//printf("server: got connection from %s\n", ip_read);

		if (!fork())
		{
			close(socket_descriptor); // child doesn't need the listener

			// extract file path
			char request[REQUEST_LENGTH];
			int received = recv(new_connection, request, REQUEST_LENGTH, 0);
			if(received == -1)
			{
				perror("recv");
				exit(1);
			}
			
			char path[REQUEST_LENGTH];
			strcpy(path, ".");
			char *space = strchr(request, ' ');
			space = space+sizeof(char);
			strcat(path, space);
			space = strchr(path, ' ');
			*space = '\0';

			// open the file
			FILE *file;
			file = fopen(path, "rb");

			if(file == NULL)
			{
				int sent = send(new_connection, "HTTP/1.0 404 Not Found\r\n\r\n", strlen("HTTP/1.0 404 Not Found\r\n\r\n"), 0);
				if(sent == -1)
				{
					perror("send");
				}
			}
			else
			{
				int sent = send(new_connection, "HTTP/1.0 200 OK\r\n\r\n", strlen("HTTP/1.0 200 OK\r\n\r\n"), 0);
				if(sent == -1)
				{
					perror("send");
				}

				char buffer[BUFFER_SEND];
				memset(&buffer, 0, sizeof buffer);
				int read;
				while( (read = fread(buffer, sizeof(char), BUFFER_SEND, file)) > 0)
				{
					if(read < BUFFER_SEND)
					{
						sent = send(new_connection, buffer, read, 0);
						if(sent == -1)
						{
							perror("send");
						}
						break;
					}
					sent = send(new_connection, buffer, BUFFER_SEND, 0);
					if(sent == -1)
					{
						perror("send");
					}
				}
			}

			fclose(file);
			//shutdown(new_connection, 2);
			close(new_connection);
			exit(0);
		}
		close(new_connection);  // parent doesn't need this
	}

	return 0;
}
