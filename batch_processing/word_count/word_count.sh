#!/bin/bash


cat $1 | tr [:space:] '\n' | grep -v "^\s*$" | sort | uniq -c | sort -bn > $2
