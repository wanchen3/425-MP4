/**
 * @file membership_client.cpp
 *
 * Client that pings each node to see if the node is alive or not
 *
 * @author Jiaqi Liu
 */
#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

#include <fstream>
#include <iostream>
#include <string>
#include <vector>
using namespace std;

/**
 * @def PORT
 * @brief The port client will be connecting to
 *
 * The membership server(server.cpp) listens on this port. The ping messages are sent to this port
 */
#define PORT "3490"

/**
 * @def PERIOD
 * @brief The time period of pinging other nodes
 *
 * Every "PERIOD" seconds, the client pings all other nodes iteratively and updates its membership list
 */
#define PERIOD 0

/**
 * Get sockaddr - IPv4 or IPv6 
 *
 * @param socket_address
 */
void *get_in_addr(struct sockaddr *socket_address)
{
	if(socket_address->sa_family == AF_INET)
	{
		return &( ((struct sockaddr_in*)socket_address)->sin_addr );
	}
	else
	{
		return &( ((struct sockaddr_in6*)socket_address)->sin6_addr );
	}
}

/**
 * 
 */
int main(void)
{
	/**
	 * Set write lock for membership list file "member.txt"
	 */
	struct flock fl;
	int fd;
	fl.l_type = F_WRLCK;
	fl.l_whence = SEEK_SET;
	fl.l_start = 0;
	fl.l_len = 0;
	fl.l_pid = getpid();
	
	/**
	 * Read "IP.txt" to get all IP's and put them in a vector
	 *
	 * @todo Test content of IPs
	 */
	vector<string> IPs;
	ifstream input("../logger/IP.txt");
	if(input.is_open())
	{
		string line = "";
		while(getline(input, line, '\n'))
		{
			IPs.push_back(line);
		}
	}
	else
	{
		cout << "Membership client - cannot open IP.txt" << endl;
		exit(EXIT_FAILURE);
	}
	input.close();
	input.clear();

	/**
	 * Update member ship list every "PERIOD" amount of seconds
	 *
	 * @todo Add unit test, including file lock 
	 */
	vector<string> membership_list; // alive node
	time_t now = time(NULL);
	time_t past = now;
	while(1)
	{
		if(now - past >= PERIOD)
		{
			past = now;

			// ping each IP
			for(int i = 0; i < IPs.size(); i = i+1)
			{
				// define hints;
				struct addrinfo hints;
				memset(&hints, 0, sizeof hints); 
				hints.ai_family = AF_UNSPEC;
				hints.ai_socktype = SOCK_STREAM;

				int result;
				struct addrinfo *server_information;
				if( (result = getaddrinfo(IPs[i].c_str(), PORT, &hints, &server_information)) != 0 )
				{
					fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(result));
					return 1;
				}

				// loop through all the results and connect to the first we can
				struct addrinfo *temp;
				int socket_descriptor;
				for(temp = server_information; temp != NULL; temp = temp->ai_next)
				{
					// get socket descriptor
					if( (socket_descriptor = socket(temp->ai_family, temp->ai_socktype, temp->ai_protocol)) == -1 )
					{
						perror("client: socket");
						continue;
					}

					// connect socket to server
					if(connect(socket_descriptor, temp->ai_addr, temp->ai_addrlen) == -1)
					{
						close(socket_descriptor);
						continue;
					}

					break;
				}

				if(temp != NULL) // node with IP[i] is still up
				{
					membership_list.push_back(IPs[i]);
				}

				close(socket_descriptor);
			}

			// flush membership_list to member.txt
			fd = open("../logger/member.txt", O_WRONLY | O_TRUNC);
			fcntl(fd, F_SETLKW, &fl); // wait and grap write lock

			for(int i = 0; i < membership_list.size(); i = i+1)
			{
				char temp[membership_list[i].size()+1];
				memset(&temp, 0, sizeof(temp));

				int j;
				int length = (int)membership_list[i].size();
				for(j = 0; j < length; j = j+1)
				{
					temp[j] = (char)membership_list[i][j];
				}
				temp[j] = '\n';

				write( fd, temp, sizeof(temp) );
			}

			fl.l_type = F_UNLCK;
			fcntl(fd, F_SETLK, &fl); // release lock
			close(fd);

			membership_list.clear(); // clear for the next iteration
		}
		now = time(NULL);
	}

	return 0;
}
